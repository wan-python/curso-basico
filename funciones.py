temas = [
    "Blanco",
    "Negro",
    "Azul"
]

def obtener_tema(tema="Azul"):
    return tema

def sumar(val1: int = 0, val2: int = 1) -> int:
    return val1 + val2

def obtener_informacion_de_juan():
    return ('Juan', 28)


if __name__ == "__main__":
    #name, edad = obtener_informacion_de_juan()
    data = obtener_informacion_de_juan()
    print(f'Mi nombre es {data[0]}')
    print(f'Y tengo {data[1]} anios')