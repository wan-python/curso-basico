def signo_mas():
    r = 4 + 3
    print(r)
    r = 4.5 + 4.5
    print(r)
    r = 4 + 4.0
    print(r)
    r = "Juan" + "Salinas"
    print(r)
    r = True + True
    print(r)
    r = [10,20, 1] + [4,5,6]
    print(r)

def signo_menos():
    r = 1 - 4
    print(r)
    r = 1.5 - 4
    print(r)
    r = True - True
    print(r)

def signo_multiplicacion():
    r = 1 * 2
    print(r)
    r = 1.5 * 4
    print(r)
    r = True * False
    print(r)
    r = [1,2,3] * 3
    print(r)

def signo_division():
    integer1 = 6
    integer2 = 2
    r = integer1/integer2
    print(r)
    # NOTAL: Si usas // y la division contiene valores flotantes entonces el resultado sera un flotante
    r = 3//2
    print(r)
    r = True/True
    print(r)


if __name__ == '__main__':
    r = 3 > 2 and True
    print(r)
    r = False or 3 > 2
    print(r)
    r = 4 == 4
    print(r)
    r = 4 == '4'
    print(r)

    r = 4%2
    print(r)

    r = not True
    print(r)

