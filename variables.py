if __name__ == '__main__':
    # Por convencion las constantes en python se declaran en mayusculas
    UNA_CONSTANTE= 44

    # Por convencion para definir attributos privados se usa gion bajo al inicio del nombre de la variable
    _valor_privado = "Secret value"

    un_entero = 1
    un_string = 'Juan' #"Juan"
    un_boleano_falso = False
    un_boleano_verdadero = True
    un_flotante_double = 1.55
    # A Las listar podemos cambiar el valor a uno de sus elementos o
    # tambien podemos agregar nuevos elementos a la coleccion y eliminar
    lista = [10, 3, 4, 5, 15]
    diccionario = {"name": "Juan", "edad": 28}
    set_de_datos = {1, 3, 4, 5}

    set_de_tuplas = {("name", "Juan"), ("edad", 28)}

    # No podemos cambiar el valor de ningun indice
    # No podemos agregar ni eliminar elementos
    tupla = (1, 3, 4, 5)
